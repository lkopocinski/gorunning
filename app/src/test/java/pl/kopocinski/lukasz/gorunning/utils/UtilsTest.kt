package pl.kopocinski.lukasz.gorunning.utils

import junit.framework.Assert.assertEquals
import org.junit.Test


class UtilsTest {

    @Test
    fun countAveragePaceTest() {
        val pace = Utils.countAveragePace(1620000, 5000.0)
        val paceString = Utils.averagePaceToString(pace)

        assertEquals(paceString, "5:24")
    }

}