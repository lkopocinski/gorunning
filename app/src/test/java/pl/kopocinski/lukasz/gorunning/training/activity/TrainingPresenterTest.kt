package pl.kopocinski.lukasz.gorunning.training.activity

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import pl.kopocinski.lukasz.gorunning.training.service.TrainingInteractor

class TrainingPresenterTest {

    lateinit var trainingInteractor: TrainingInteractor
    lateinit var view: TrainingView
    lateinit var presenter: TrainingPresenter

    @Before
    fun setUp() {
        trainingInteractor = mock<TrainingInteractor>()
        view = mock<TrainingView>()

        presenter = TrainingPresenter(trainingInteractor)
        presenter.attachView(view)
    }

    @Test
    fun testInitializeNewTraining() {
        presenter.initialize(true)

        verify(view).showTrainingParameters()
        verify(view).setTrainingActionsCallback(any())
        verify(trainingInteractor).startTraining()
    }
}