package pl.kopocinski.lukasz.gorunning.training.service.section_actions;

public abstract class SectionAction {
    public abstract void perform(SectionActions actions);
}
