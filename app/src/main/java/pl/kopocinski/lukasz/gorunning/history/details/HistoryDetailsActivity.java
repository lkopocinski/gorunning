package pl.kopocinski.lukasz.gorunning.history.details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.TrainingResultsMapActivity;
import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.layout.simple_list_item_1;

public class HistoryDetailsActivity
        extends MvpActivity<HistoryDetailsActivityView, HistoryDetailsActivityPresenter>
        implements HistoryDetailsActivityView {

    public static final String DATE_EXTRA = "date";

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.sections_list) ListView sectionsListView;

    private HorizontalInfiniteCycleViewPager infiniteCycleViewPager;

    public static void start(Context context, @NonNull Training selectedTraining) {
        final Intent intent = new Intent(context, HistoryDetailsActivity.class);
        intent.putExtra(DATE_EXTRA, selectedTraining.getDate());
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        initView();
    }

    private void initView() {
        Date selectedTrainingDate = getSelectedTrainingDate(getIntent());
        presenter.loadTrainings(selectedTrainingDate);
    }

    private Date getSelectedTrainingDate(Intent intent) {
        return (Date) intent.getSerializableExtra(DATE_EXTRA);
    }

    @Override
    public void showTrainings(List<Training> trainingsList, int current) {
        HorizontalPagerAdapter adapter = new HorizontalPagerAdapter(trainingsList);
        adapter.onMapClick().subscribe(this::showOnMap);
        adapter.onSectionClick().subscribe(presenter::showSectionResults);
        setUpViewPager(adapter, current);
    }

    private void showOnMap(Training training) {
        TrainingResultsMapActivity.start(this, training);
    }

    private void setUpViewPager(HorizontalPagerAdapter adapter, int current) {
        infiniteCycleViewPager = (HorizontalInfiniteCycleViewPager) findViewById(R.id.view_pager);
        infiniteCycleViewPager.setAdapter(adapter);
        infiniteCycleViewPager.setCurrentItem(current);
    }

    @NonNull
    @Override
    public HistoryDetailsActivityPresenter createPresenter() {
        return new HistoryDetailsActivityPresenter();
    }

    @Override
    public void showSectionsResults(List<String> resultList) {
        sectionsListView.setAdapter(new ArrayAdapter<>(this, simple_list_item_1, resultList));
        infiniteCycleViewPager.setVisibility(View.INVISIBLE);
        sectionsListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (infiniteCycleViewPager.getVisibility() == View.INVISIBLE) {
            infiniteCycleViewPager.setVisibility(View.VISIBLE);
            sectionsListView.setVisibility(View.INVISIBLE);
        } else {
            super.onBackPressed();
        }
    }
}
