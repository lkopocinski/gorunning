package pl.kopocinski.lukasz.gorunning.training.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.dagger.modules.FragmentModule;
import pl.kopocinski.lukasz.gorunning.training.activity.TrainingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TrainingFragment extends MvpFragment<TrainingFragmentView, TrainingFragmentPresenter>
        implements TrainingFragmentView {

    @BindView(R.id.text_duration) TextView durationText;
    @BindView(R.id.text_average_pace) TextView averagePaceText;
    @BindView(R.id.text_distance) TextView distanceText;

    private Unbinder unbinder;

    public TrainingFragment() {
        // Required empty public constructor
    }

    public static TrainingFragment newInstance() {
        return new TrainingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_training, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startViewUpdates();
    }

    @Override
    public void setDuration(String time) {
        durationText.setText(time);
    }

    @Override
    public void setAveragePace(String averagePace) {
        averagePaceText.setText(averagePace);
    }

    @Override
    public void setDistance(String distance) {
        distanceText.setText(distance);
    }

    @Override
    public void onStop() {
        presenter.stopViewUpdates();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @NonNull
    @Override
    public TrainingFragmentPresenter createPresenter() {
        return ((TrainingActivity) getActivity()).getActivityComponent()
                .fragmentComponent(new FragmentModule())
                .trainingFragmentPresenter();
    }
}
