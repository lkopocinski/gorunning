package pl.kopocinski.lukasz.gorunning.utils;

import android.location.GnssMeasurementsEvent;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.orhanobut.logger.Logger;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.PublishSubject;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.location.LocationManager.GPS_PROVIDER;

public class ProviderGPS implements LocationListener {

    private final static String GPS = "gps";

    private LocationManager locationManager;
    private RxPermissions rxPermissions;

    private PublishSubject<Boolean> gpsEnabledSubject;

    @Inject
    public ProviderGPS(LocationManager locationManager, RxPermissions rxPermissions) {
        this.locationManager = locationManager;
        this.rxPermissions = rxPermissions;

        gpsEnabledSubject = PublishSubject.create();
    }


    public boolean isEnabled() {
        return locationManager.isProviderEnabled(GPS_PROVIDER);
    }

    public int getAccuracy() {
        return locationManager.getProvider(GPS_PROVIDER).getAccuracy();
    }

    public ProviderGPS requestUpdates() {
        if (rxPermissions.isGranted(ACCESS_FINE_LOCATION)) {
            locationManager.requestLocationUpdates(GPS_PROVIDER, 0, 0, this);
        }

        return this;
    }

    @SuppressWarnings("MissingPermission")
    public void removeUpdates() {
        if (rxPermissions.isGranted(ACCESS_FINE_LOCATION)) {
            locationManager.removeUpdates(this);
        }
    }

    public Observable<Boolean> retrieveEnabledStatus() {
        return gpsEnabledSubject;
    }

    @Override
    public void onProviderEnabled(String provider) {
        if (provider.equals(GPS)) {
            gpsEnabledSubject.onNext(true);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (provider.equals(GPS)) {
            gpsEnabledSubject.onNext(false);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Logger.d(location.getAccuracy());
        final int accuracy = getAccuracy();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle bundle) {
        Logger.d(provider, status);
    }
}
