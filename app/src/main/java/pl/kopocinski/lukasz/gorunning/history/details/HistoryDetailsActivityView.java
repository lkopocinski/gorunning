package pl.kopocinski.lukasz.gorunning.history.details;

import com.hannesdorfmann.mosby.mvp.MvpView;

import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.List;

interface HistoryDetailsActivityView extends MvpView {

    void showTrainings(List<Training> trainingsList, int current);

    void showSectionsResults(List<String> resultList);
}
