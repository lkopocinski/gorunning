package pl.kopocinski.lukasz.gorunning.training.activity;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import pl.kopocinski.lukasz.gorunning.custom_views.ButtonTraining;
import pl.kopocinski.lukasz.gorunning.training.service.TrainingInteractor;

import javax.inject.Inject;

public class TrainingPresenter extends MvpBasePresenter<TrainingView> {

    private TrainingInteractor trainingInteractor;

    @Inject
    TrainingPresenter(TrainingInteractor trainingInteractor) {
        this.trainingInteractor = trainingInteractor;
    }

    void initialize(boolean isNewTraining) {
        if (isViewAttached()) {
            getView().setTrainingActionsCallback(getActionsCallback());
        }

        if (isNewTraining) {
            startTraining();
        } else {
            continueTraining();
        }
    }

    private ButtonTraining.ActionsCallback getActionsCallback() {
        return new ButtonTraining.ActionsCallback() {
            @Override
            public void onActionPause() {
                trainingInteractor.pauseTraining();
            }

            @Override
            public void onActionResume() {
                trainingInteractor.resumeTraining();
            }

            @Override
            public void onActionStop() {
                if (isViewAttached()) {
                    trainingInteractor.finishTraining();
                    getView().showTrainingResults();
                }
            }
        };
    }

    private void startTraining() {
        if (isViewAttached()) {
            getView().showTrainingParameters();
            trainingInteractor.startTraining();
        }
    }

    private void continueTraining() {
        if (isViewAttached()) {
            getView().showTrainingParameters();
        }
    }

    void buttonStartSectionClicked() {
        if (isViewAttached()) {
            getView().setButtonStartSectionEnabled(false);
            getView().setButtonStopSectionEnabled(true);
        }
        trainingInteractor.startSection();
    }

    void buttonStopSectionClicked() {
        if (isViewAttached()) {
            getView().setButtonStartSectionEnabled(true);
            getView().setButtonStopSectionEnabled(false);
        }
        trainingInteractor.stopSection();
    }

    void attach() {
        hideNotification();
    }

    void detach() {
        showNotification();
    }

    private void showNotification() {
        trainingInteractor.doForeground();
    }

    private void hideNotification() {
        trainingInteractor.doBackground();
    }

    void onExit() {
        if (isViewAttached()) {
            getView().showStopTrainingAlert();
        }
    }

    void onAlertPositiveButtonClicked() {
        trainingInteractor.finishTraining();
    }
}

