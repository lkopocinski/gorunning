package pl.kopocinski.lukasz.gorunning.map;

import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;

import java.util.List;

public interface Map {
    void zoomLocation(Coordinates coordinates, int zoomSize);

    void hideMap();

    void printRoute(List<Coordinates> coordinatesList);

    void printSection(List<Coordinates> coordinatesList);

    void removeCallbacks();
}
