package pl.kopocinski.lukasz.gorunning;

import com.google.android.gms.maps.model.LatLng;

import android.content.Context;

import java.util.List;

public class MapImageUrlProvider {

    private static final String STATIC_MAP_ENDPOINT = "https://maps.googleapis.com/maps/api/staticmap";
    private static final String SIZE = "512x256";

    private static final String POLY_WEIGHT = "5";
    private static final String POLY_COLOR = "0xggccff";

    private final String mapURL;

    public MapImageUrlProvider(Context context, List<LatLng> route) {
        String apiKey = context.getString(R.string.google_maps_key);
        String encodedPoly = MyPolyUtil.encode(route);

        String path = String.format("weight:%s|color:%s|enc:%s",
                POLY_WEIGHT,
                POLY_COLOR,
                encodedPoly);

        mapURL = String.format("%s?size=%s&path=%s&key=%s",
                STATIC_MAP_ENDPOINT,
                SIZE,
                path,
                apiKey);
    }

    public String getMapUrl() {
        return mapURL;
    }
}
