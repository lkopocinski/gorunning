package pl.kopocinski.lukasz.gorunning.training.service.section_actions;



public interface SectionActions {
    void startSection();
    void stopSection();
}
