package pl.kopocinski.lukasz.gorunning.dagger.modules;

import android.location.LocationManager;

import com.tbruyelle.rxpermissions.RxPermissions;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import pl.kopocinski.lukasz.gorunning.App;
import pl.kopocinski.lukasz.gorunning.training.service.TrainingInteractor;
import pl.kopocinski.lukasz.gorunning.training.service.TrainingServiceInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.LOCATION_SERVICE;


@Module
public class LocationModule {
    private final App application;

    public LocationModule(App application) {
        this.application = application;
    }

    @Provides
    @Singleton
    LocationManager provideLocationManager() {
        return (LocationManager) application.getSystemService(LOCATION_SERVICE);
    }

    @Provides
    @Singleton
    ReactiveLocationProvider provideLocationProvider() {
        return new ReactiveLocationProvider(application);
    }

    @Provides
    @Singleton
    RxPermissions provideRxPermissions() {
        return RxPermissions.getInstance(application);
    }

    @Provides
    @Singleton
    TrainingInteractor provideTrainingServiceInteractor() {
        return new TrainingServiceInteractor(application);
    }
}
