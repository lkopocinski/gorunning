package pl.kopocinski.lukasz.gorunning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import pl.kopocinski.lukasz.gorunning.database.model.Section;
import pl.kopocinski.lukasz.gorunning.database.model.Training;
import pl.kopocinski.lukasz.gorunning.map.GoogleMap;
import pl.kopocinski.lukasz.gorunning.map.Map;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class TrainingResultsMapActivity extends AppCompatActivity implements GoogleMap.OnMapReadyCallback {

    public static final String DATE_EXTRA = "date";
    @BindView(R.id.GPS_accuracy) TextView GPSAccuracy;

    private Map map;

    public static void start(Context context, Training training) {
        final Intent intent = new Intent(context, TrainingResultsMapActivity.class);
        intent.putExtra(DATE_EXTRA, training.getDate());
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_results_map);
        ButterKnife.bind(this);

        showMap();
    }

    private void showMap() {
        map = new GoogleMap.Builder(this, R.id.map)
                .mapStyle(R.raw.map_style_beryl)
                .onMapReady(this)
                .build();
    }

    private Date getTrainingDate(Intent intent) {
        if (intent == null || intent.getExtras() == null) {
            return null;
        } else {
            return (Date) intent.getSerializableExtra(DATE_EXTRA);
        }
    }

    @Override
    public void onMapReady() {
        printRoute();
        printSections();
    }

    private void printRoute() {
        Realm realm = Realm.getDefaultInstance();

        Date date = getTrainingDate(getIntent());

        Training training = realm
                .where(Training.class)
                .equalTo("date", date)
                .findFirst();

        map.printRoute(training.getRoute());
        String accuracyText = String.format("Dokładność GPS: %s m", training.getAverageAccuracy());
        GPSAccuracy.setText(accuracyText);
    }

    private void printSections() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Section> sections = realm.where(Section.class).findAll();
        for (Section section : sections) {
            map.printSection(section.getSectionRoute());
        }
    }
}
