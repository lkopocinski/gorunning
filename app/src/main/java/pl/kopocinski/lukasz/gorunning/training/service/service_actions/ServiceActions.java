package pl.kopocinski.lukasz.gorunning.training.service.service_actions;

public interface ServiceActions {
    void doForeground();

    void doBackground();
}
