package pl.kopocinski.lukasz.gorunning.dagger.modules;

import pl.kopocinski.lukasz.gorunning.App;

import dagger.Module;

@Module
public class AppModule {
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }
}
