package pl.kopocinski.lukasz.gorunning.history.overview;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

import static io.realm.Sort.DESCENDING;

public class HistoryActivityPresenter extends MvpBasePresenter<HistoryActivityView> {

    private Realm realmInstance;

    @Inject
    HistoryActivityPresenter() {
        realmInstance = Realm.getDefaultInstance();
    }

    void loadTrainings() {
        List<Training> trainingsList = getAllTrainings();

        if (trainingsList.isEmpty()) {
            showNoTrainingsInfo();
        } else {
            showTrainings(trainingsList);
        }
    }

    private List<Training> getAllTrainings(){
        return realmInstance.where(Training.class).findAllSorted(Training.DATE, DESCENDING);
    }

    private void showNoTrainingsInfo() {
        if (isViewAttached()) {
            getView().showNoTrainingsInfo();
        }
    }

    private void showTrainings(List<Training> trainingsList) {
        if (isViewAttached()) {
            getView().showTrainings(trainingsList);
        }
    }
}
