package pl.kopocinski.lukasz.gorunning.dagger.components;

import pl.kopocinski.lukasz.gorunning.dagger.modules.ActivityModule;
import pl.kopocinski.lukasz.gorunning.dagger.modules.AppModule;
import pl.kopocinski.lukasz.gorunning.dagger.modules.LocationModule;
import pl.kopocinski.lukasz.gorunning.training.service.TrainingService;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class, LocationModule.class})
public interface AppComponent {

    void inject(TrainingService service);

    ActivityComponent activityComponent(ActivityModule module);
}
