package pl.kopocinski.lukasz.gorunning.training.service.section_actions;

import org.parceler.Parcel;

@Parcel
public class StopSection extends SectionAction {
    @Override
    public void perform(SectionActions actions) {
        actions.stopSection();
    }
}
