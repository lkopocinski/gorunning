package pl.kopocinski.lukasz.gorunning.history.details;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import pl.kopocinski.lukasz.gorunning.MapImageUrlProvider;
import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

class HorizontalPagerAdapter extends PagerAdapter {

    private final PublishSubject<Training> onMapClickSubject = PublishSubject.create();
    private final PublishSubject<Training> onSectionsClickSubject = PublishSubject.create();

    private List<Training> trainingsList;

    HorizontalPagerAdapter(@NonNull List<Training> trainingsList) {
        this.trainingsList = trainingsList;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view = LayoutInflater
                .from(container.getContext())
                .inflate(R.layout.results_card, container, false);

        setupItem(new TrainingDetailsHolder(view), trainingsList.get(position));

        container.addView(view);
        return view;
    }

    private void setupItem(final TrainingDetailsHolder holder, final Training training) {
        holder.day.setText(training.getDayReadable());
        holder.dayTime.setText(training.getDateWithHourReadable());
        holder.distance.setText(training.getDistanceString());
        holder.duration.setText(training.getDurationString());
        holder.averagePace.setText(training.getAveragePaceString());

        holder.sections.setOnClickListener(view -> onSectionsClickSubject.onNext(training));
        holder.mapImage.setOnClickListener(view -> onMapClickSubject.onNext(training));

        setMapWithDrawnRoute(holder.mapImage, training);
    }

    private void setMapWithDrawnRoute(ImageView imageView, Training training) {
        MapImageUrlProvider urlProvider =
                new MapImageUrlProvider(imageView.getContext(), training.getRouteLatLng());

        Picasso.with(imageView.getContext())
                .load(urlProvider.getMapUrl())
                .error(R.drawable.google_maps_placeholder)
                .placeholder(R.drawable.google_maps_placeholder)
                .into(imageView);
    }

    public void setList(List<Training> newTrainingsList) {
        trainingsList.clear();
        trainingsList.addAll(newTrainingsList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return trainingsList.size();
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }

    Observable<Training> onMapClick() {
        return onMapClickSubject.asObservable();
    }

    Observable<Training> onSectionClick() {
        return onSectionsClickSubject.asObservable();
    }

    class TrainingDetailsHolder {
        @BindView(R.id.mapImage) ImageView mapImage;
        @BindView(R.id.day) TextView day;
        @BindView(R.id.date_time) TextView dayTime;
        @BindView(R.id.distance) TextView distance;
        @BindView(R.id.duration) TextView duration;
        @BindView(R.id.average_pace) TextView averagePace;
        @BindView(R.id.sections) TextView sections;

        TrainingDetailsHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
