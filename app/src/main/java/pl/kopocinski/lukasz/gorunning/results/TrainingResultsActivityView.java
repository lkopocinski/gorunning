package pl.kopocinski.lukasz.gorunning.results;

import com.google.android.gms.maps.model.LatLng;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

interface TrainingResultsActivityView extends MvpView {

    void setMapImage(List<LatLng> route);

    void setDay(String day);

    void setDate(String date);

    void setDistance(String distance);

    void setDuration(String duration);

    void setAveragePace(String averagePace);

    void onTrainingDeleted();

    void showDeleteTrainingAlert();
}
