package pl.kopocinski.lukasz.gorunning.training.fragment;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Łukasz on 2016-11-14.
 */

public interface TrainingFragmentView extends MvpView {

    void setDuration(String duration);

    void setAveragePace(String pace);

    void setDistance(String distance);
}
