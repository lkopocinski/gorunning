package pl.kopocinski.lukasz.gorunning.training.fragment_training_map;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orhanobut.logger.Logger;

import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;
import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.google.android.gms.internal.zzs.TAG;

public class TrainingFragmentMap extends SupportMapFragment implements OnMapReadyCallback {

    private GoogleMap map;

    public TrainingFragmentMap() {
        super();
    }

    public static TrainingFragmentMap newInstance() {
        return new TrainingFragmentMap();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initMap();

        return view;
    }

    private void initMap() {
        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //googleMap.setMyLocationEnabled(true);
        map = googleMap;

        styleMap();
        printRoute(googleMap);
    }

    public void styleMap() {
        try {
            boolean success = map.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.map_style_sin_sity));

            if (!success) {
                Logger.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Logger.e(TAG, "Can't find style. Error: ", e);
        }

    }

    public void printRoute(GoogleMap googleMap) {

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Training> list = realm.where(Training.class).equalTo("active", true).findAllSorted("id", Sort.DESCENDING);
        RealmList<Coordinates> route = list.get(0).getRoute();
        printPolyLine(route);

        list.addChangeListener(element -> printPolyLine(element.get(0).getRoute()));
    }

    public void printPolyLine(List<Coordinates> coordinatesList) {
        List<LatLng> latLngList = new ArrayList<>();
        for (Coordinates coordinates : coordinatesList) {
            latLngList.add(new LatLng(coordinates.getLatitude(),coordinates.getLongitude()));
        }

        PolylineOptions polyline = new PolylineOptions()
                .width(12)
                .addAll(latLngList)
                .color(Color.GREEN);

        map.addPolyline(polyline);

        map.addMarker(new MarkerOptions()
                .position(latLngList.get(0))
                .title("Start training"));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngList.get(0), 16));
    }
}
