package pl.kopocinski.lukasz.gorunning.training.fragment;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import pl.kopocinski.lukasz.gorunning.database.model.Training;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static io.realm.Sort.DESCENDING;

public class TrainingFragmentPresenter extends MvpBasePresenter<TrainingFragmentView>
        implements RealmChangeListener<Realm> {

    private static final int FIRST_ELEMENT = 0;

    private Realm realmInstance;

    @Inject
    TrainingFragmentPresenter() {}

    void startViewUpdates() {
        realmInstance = Realm.getDefaultInstance();
        realmInstance.addChangeListener(this);
    }

    @Override
    public void onChange(Realm realm) {
        final RealmResults<Training> all = realm.where(Training.class).findAll();

        Training activeTraining = getActiveTraining(realm);
        if (activeTraining != null) {
            updateView(activeTraining);
        }
    }

    private Training getActiveTraining(Realm realm) {
        RealmResults<Training> trainingsList = realm
                .where(Training.class)
                .equalTo("active", true)
                .findAllSorted("id", DESCENDING);

        if (trainingsList.isEmpty()) return null;

        return trainingsList.get(FIRST_ELEMENT);
    }

    private void updateView(Training training) {
        if (isViewAttached()) {
            getView().setDuration(training.getDurationString());
            getView().setDistance(training.getDistanceString());
            getView().setAveragePace(training.getAveragePaceString());
        }
    }

    void stopViewUpdates() {
        realmInstance.removeChangeListener(this);
        realmInstance.close();
    }
}
