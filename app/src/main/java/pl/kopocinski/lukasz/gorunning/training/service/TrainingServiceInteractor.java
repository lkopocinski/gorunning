package pl.kopocinski.lukasz.gorunning.training.service;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import org.parceler.Parcels;

import pl.kopocinski.lukasz.gorunning.training.service.section_actions.SectionAction;
import pl.kopocinski.lukasz.gorunning.training.service.section_actions.SectionActions;
import pl.kopocinski.lukasz.gorunning.training.service.section_actions.StartSection;
import pl.kopocinski.lukasz.gorunning.training.service.section_actions.StopSection;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.Background;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.Foreground;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.ServiceAction;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.ServiceActions;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.PauseTraining;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.ResumeTraining;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.StartTraining;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.StopTraining;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.TrainingAction;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.TrainingActions;

import static pl.kopocinski.lukasz.gorunning.training.service.TrainingService.SECTION_ACTION;
import static pl.kopocinski.lukasz.gorunning.training.service.TrainingService.SERVICE_ACTION;
import static pl.kopocinski.lukasz.gorunning.training.service.TrainingService.TRAINING_ACTION;

public class TrainingServiceInteractor implements TrainingInteractor {
    private boolean isStarted = false;
    private boolean isPaused = false;

    private Context context;

    public TrainingServiceInteractor(Context context) {
        this.context = context;
    }

    @Override
    public void startTraining() {
        if (!isStarted) {
            performAction(new StartTraining());
            setStarted(true);
        }
    }

    @Override
    public void pauseTraining() {
        if (isStarted) {
            performAction(new PauseTraining());
            setPaused(true);
        }
    }

    @Override
    public void resumeTraining() {
        if (isPaused) {
            performAction(new ResumeTraining());
            setPaused(false);
        }
    }

    @Override
    public void finishTraining() {
        if (isStarted) {
            performAction(new StopTraining());
            setStarted(false);
        }
    }

    @Override
    public void doForeground() {
        if (isStarted) {
            performAction(new Foreground());
        }
    }

    @Override
    public void doBackground() {
        performAction(new Background());
    }

    @Override
    public void startSection() {
        if(isStarted){
            performAction(new StartSection());
        }
    }

    @Override
    public void stopSection() {
        if(isStarted){
            performAction(new StopSection());
        }
    }

    private void performAction(TrainingAction action) {
        Parcelable wrapped = Parcels.wrap(action);

        Intent i = new Intent(context, TrainingService.class);
        i.putExtra(TRAINING_ACTION, wrapped);

        context.startService(i);
    }

    private void performAction(ServiceAction action) {
        Parcelable wrapped = Parcels.wrap(action);

        Intent i = new Intent(context, TrainingService.class);
        i.putExtra(SERVICE_ACTION, wrapped);

        context.startService(i);
    }

    private void performAction(SectionAction action) {
        Parcelable wrapped = Parcels.wrap(action);

        Intent i = new Intent(context, TrainingService.class);
        i.putExtra(SECTION_ACTION, wrapped);

        context.startService(i);
    }

    private void setStarted(boolean isStarted) {
        this.isStarted = isStarted;
    }

    private void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }
}
