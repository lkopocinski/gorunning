package pl.kopocinski.lukasz.gorunning;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

import pl.kopocinski.lukasz.gorunning.dagger.components.ActivityComponent;
import pl.kopocinski.lukasz.gorunning.dagger.modules.ActivityModule;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import butterknife.ButterKnife;

public abstract class BaseActivity<V extends MvpView, P extends MvpPresenter<V>>
        extends MvpActivity<V, P> {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activityComponent = createComponent();
        super.onCreate(savedInstanceState);

        setContentView(getLayoutRes());
        ButterKnife.bind(this);
    }

    private ActivityComponent createComponent() {
        return ((App) getApplication()).appComponent()
                .activityComponent(new ActivityModule(this));
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @LayoutRes
    protected abstract int getLayoutRes();
}
