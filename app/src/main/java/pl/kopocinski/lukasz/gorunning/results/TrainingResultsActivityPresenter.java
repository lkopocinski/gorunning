package pl.kopocinski.lukasz.gorunning.results;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

import static io.realm.Sort.DESCENDING;

public class TrainingResultsActivityPresenter extends MvpBasePresenter<TrainingResultsActivityView> {

    private static final int FIRST_ELEMENT = 0;

    private Realm realmInstance;

    @Inject
    TrainingResultsActivityPresenter() {
        realmInstance = Realm.getDefaultInstance();
    }

    void showResults() {
        List<Training> trainingsList = getAllTrainings();
        showTrainingResults(trainingsList.get(FIRST_ELEMENT));
    }

    private List<Training> getAllTrainings() {
        return realmInstance.where(Training.class).findAllSorted(Training.ID, DESCENDING);
    }

    private void showTrainingResults(Training training) {
        if (isViewAttached()) {
            getView().setDay(training.getDayReadable());
            getView().setDate(training.getDateWithHourReadable());
            getView().setDistance(training.getDistanceString());
            getView().setDuration(training.getDurationString());
            getView().setAveragePace(training.getAveragePaceString());
            getView().setMapImage(training.getRouteLatLng());
        }
    }

    void deleteTraining() {
        realmInstance.executeTransaction(realm -> {
            Training training = realm
                    .where(Training.class)
                    .findAllSorted(Training.ID, DESCENDING)
                    .first();

            training.deleteFromRealm();
        });

        if (isViewAttached()) {
            getView().onTrainingDeleted();
        }
    }

    public void onBinIconClicked() {
        if(isViewAttached()){
            getView().showDeleteTrainingAlert();
        }
    }
}
