package pl.kopocinski.lukasz.gorunning.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.hannesdorfmann.mosby.mvp.MvpActivity;

import pl.kopocinski.lukasz.gorunning.App;
import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.dagger.modules.ActivityModule;
import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;
import pl.kopocinski.lukasz.gorunning.history.overview.HistoryActivity;
import pl.kopocinski.lukasz.gorunning.map.GoogleMap;
import pl.kopocinski.lukasz.gorunning.map.Map;
import pl.kopocinski.lukasz.gorunning.training.activity.TrainingActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpActivity<MainActivityView, MainActivityPresenter>
        implements MainActivityView, GoogleMap.OnMapReadyCallback {

    @BindView(R.id.toolbar) Toolbar toolbar;

    private Map map;
    private AlertDialog gpsDisabledDialog;

    public static void start(Context context) {
        final Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.initialize();
    }

    @Override
    public void showMap() {
        map = new GoogleMap.Builder(this, R.id.map)
                .mapStyle(R.raw.map_style_beryl)
                .myLocationModule(true)
                .onMapReady(this)
                .build();
    }

    @Override
    public void onMapReady() {
        presenter.onMapReady();
    }

    @Override
    public void showLocation(Coordinates coordinates) {
        map.zoomLocation(coordinates, 17);
    }

    @Override
    public void showGPSDisabledInfo() {
        if (gpsDisabledDialog == null) {
            gpsDisabledDialog = getGPSDisabledDialog();
        }

        if (!gpsDisabledDialog.isShowing()) {
            gpsDisabledDialog.show();
        }
    }

    private AlertDialog getGPSDisabledDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(R.string.gps_disabled)
                .setMessage(R.string.gps_disabled_rationale)
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> launchLocationSettings())
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                })
                .setCancelable(false)
                .create();
    }

    @Override
    public void hideGPSDisabledInfo() {
        if (gpsDisabledDialog.isShowing()) {
            gpsDisabledDialog.dismiss();
        }
    }

    private void launchLocationSettings() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.button_go)
    public void OnClick() {
        TrainingActivity.startWithNewTraining(this);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_history) {
            HistoryActivity.start(this);
        }

        return true;
    }

    @Override
    protected void onStop() {
        presenter.terminate();
        map.removeCallbacks();
        super.onStop();
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return ((App) getApplication()).appComponent()
                .activityComponent(new ActivityModule(this))
                .mainPresenter();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}