package pl.kopocinski.lukasz.gorunning.custom_views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import pl.kopocinski.lukasz.gorunning.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ButtonTraining extends FrameLayout {

    @BindView(R.id.text_pause)
    TextView pauseButton;

    @BindView(R.id.text_stop)
    TextView stopButton;

    @BindView(R.id.text_resume)
    TextView resumeButton;

    String textPause;
    String textStop;
    String textResume;

    int colorPause;
    int colorStop;
    int colorResume;

    public ButtonTraining(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ButtonTraining);
        initAttributes(typedArray, context);

        View view = LayoutInflater.from(context).inflate(R.layout.button_training, this, true);
        ButterKnife.bind(this, view);

        setAttributes();
    }

    private void initAttributes(TypedArray typedArray, Context context) {
        textPause = typedArray.getString(R.styleable.ButtonTraining_textPause);
        textStop = typedArray.getString(R.styleable.ButtonTraining_textStop);
        textResume = typedArray.getString(R.styleable.ButtonTraining_textResume);

        colorPause = typedArray.getColor(R.styleable.ButtonTraining_colorPause,
                ContextCompat.getColor(context, android.R.color.black));

        colorStop = typedArray.getColor(R.styleable.ButtonTraining_colorStop,
                ContextCompat.getColor(context, android.R.color.black));

        colorResume = typedArray.getColor(R.styleable.ButtonTraining_colorResume,
                ContextCompat.getColor(context, android.R.color.black));

        typedArray.recycle();
    }

    private void setAttributes() {
        if (textPause != null) {
            pauseButton.setText(textPause);
        }

        if (textStop != null) {
            stopButton.setText(textStop);
        }

        if (textResume != null) {
            resumeButton.setText(textResume);
        }

        if (colorPause != 0) {
            pauseButton.setBackgroundColor(colorPause);
        }

        if (colorStop != 0) {
            stopButton.setBackgroundColor(colorStop);
        }

        if (colorResume != 0) {
            resumeButton.setBackgroundColor(colorResume);
        }

        stopButton.setTextColor(Color.WHITE);
        pauseButton.setTextColor(Color.BLACK);
        resumeButton.setTextColor(Color.WHITE);

    }

    private ActionsCallback actionListener;

    public void setActionsCallback(ActionsCallback actionListener) {
        this.actionListener = actionListener;
    }

    public interface ActionsCallback {
        void onActionPause();

        void onActionResume();

        void onActionStop();
    }

    @OnClick(R.id.text_pause)
    public void onPauseClick() {
        onPause();
        actionListener.onActionPause();
    }

    private void onPause() {
        /*pauseButton.setVisibility(INVISIBLE);
        stopButton.setVisibility(INVISIBLE);
        resumeButton.setVisibility(VISIBLE);*/
        enterReveal(resumeButton);
    }

    @OnClick(R.id.text_stop)
    public void onStopClick() {
        actionListener.onActionStop();
    }

    private void onStop() {

    }

    @OnClick(R.id.text_resume)
    public void onResumeClick() {
        onResume();
        actionListener.onActionResume();
    }

    private void onResume() {
       /* pauseButton.setVisibility(VISIBLE);
        stopButton.setVisibility(VISIBLE);
        resumeButton.setVisibility(INVISIBLE);*/
        exitReveal(resumeButton);
    }

    private static long REVEAL_DURATION = 700;

    void enterReveal(final View view) {
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });

        anim.setDuration(REVEAL_DURATION);
        anim.start();
        view.setVisibility(VISIBLE);
    }

    void exitReveal(final View view) {
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        int initialRadius = view.getWidth() / 2;

        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
            }
        });

        anim.setDuration(REVEAL_DURATION);
        anim.start();
    }

}
