package pl.kopocinski.lukasz.gorunning;


import com.google.android.gms.maps.model.LatLng;

import android.support.annotation.NonNull;

import pl.kopocinski.lukasz.gorunning.database.model.Section;
import pl.kopocinski.lukasz.gorunning.database.model.Training;
import pl.kopocinski.lukasz.gorunning.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class RouteMatcher {

    private Training actualTraining;
    private final List<Section> sections;
    private final List<LatLng> trainingRoute;

    public RouteMatcher(Training actualTraining, List<Section> sections) {
        this.actualTraining = actualTraining;
        this.sections = sections;
        this.trainingRoute = actualTraining.getRouteLatLng();
    }

    private List<SectionOnRoute> getSectionsWithStartAndStopPointOnTrainingRoute() {
        final List<SectionOnRoute> sectionsOnRoutes = new ArrayList<>();

        for (Section section : sections) {
            if (!section.getSectionRoute().isEmpty()) {
                final int startIndexOnRoute = getStartPointIndexOnTrainingRoute(section.getStartPoint(), trainingRoute);

                if (isIndexOnRoute(startIndexOnRoute)) {
                    final int stopIndexOnRoute = getStopPointIndexOnTrainingRoute(section.getStopPoint(), trainingRoute);
                    if (isIndexOnRoute(stopIndexOnRoute)) {
                        sectionsOnRoutes.add(new SectionOnRoute(startIndexOnRoute, stopIndexOnRoute, section));
                    }
                }
            }
        }

        return sectionsOnRoutes;
    }

    private int getStartPointIndexOnTrainingRoute(@NonNull LatLng startPoint, List<LatLng> trainingRoute) {
        return MyPolyUtil.locationIndexOnPath(startPoint, trainingRoute, false, 10); // TOLERANCE 0.1 m
    }

    private int getStopPointIndexOnTrainingRoute(@NonNull LatLng stopPoint, List<LatLng> trainingRoute) {
        return MyPolyUtil.locationIndexOnPath(stopPoint, trainingRoute, false, 10); // TOLERANCE 0.1 m
    }

    private boolean isIndexOnRoute(int point) {
        return point != -1;
    }

    private List<SectionOnRoute> getRealSectionsFromSectionsWithStartAndStopPointOnTrainingRoute(List<SectionOnRoute> sectionOnRouteList) {
        final List<SectionOnRoute> realSections = new ArrayList<>();

        for (SectionOnRoute sectionOnRoute : sectionOnRouteList) {
            final List<LatLng> sectionFromRoad = trainingRoute.subList(sectionOnRoute.startIndex, sectionOnRoute.stopIndex);
            final List<LatLng> section = sectionOnRoute.section.getSectionRouteLatLng();

            if (areSectionsMatch(sectionFromRoad, section)) {
                realSections.add(sectionOnRoute);
            }
        }

        return realSections;
    }

    private boolean areSectionsMatch(final List<LatLng> sectionFromRoad, final List<LatLng> section) {
        int matchIndicator = 0;

        for (LatLng latLng : sectionFromRoad) {
            if (MyPolyUtil.isLocationOnPath(latLng, section, false, 10)) {
                matchIndicator += 1;
            }
        }

        return isMatch(sectionFromRoad.size(), matchIndicator);
    }

    private boolean isMatch(double sectionSize, double matchPointsCount) {
        final double MINIMAL_MATCH_PERCENTAGE = 0.7;
        final double matchPercentage = matchPointsCount / sectionSize;
        return matchPercentage > MINIMAL_MATCH_PERCENTAGE;
    }

    public List<SectionResults> getResults() {
        final List<SectionOnRoute> sectionsWithStartAndStopPoint = getSectionsWithStartAndStopPointOnTrainingRoute();
        final List<SectionOnRoute> realSections = getRealSectionsFromSectionsWithStartAndStopPointOnTrainingRoute(sectionsWithStartAndStopPoint);

        List<SectionResults> sectionResultsList = new ArrayList<>(realSections.size());
        for (SectionOnRoute realSection : realSections) {
            final long sectionDuration = getSectionDuration(realSection);
            final long sectionFromTrainingDuration = getSectionFromTrainingDuration(realSection);

            Section section = realSection.section;
            final SectionResults sectionResults = new SectionResults(section.getName(), sectionDuration, sectionFromTrainingDuration, section.getDistance());

            sectionResultsList.add(sectionResults);
        }

        return sectionResultsList;
    }

    private long getSectionDuration(SectionOnRoute sectionOnRoute) {
        long sectionStartMillis = sectionOnRoute.section.getStartMillis();
        long sectionStopMillis = sectionOnRoute.section.getStopMillis();
        long sectionDuration = sectionStopMillis - sectionStartMillis;
        return sectionDuration;
    }

    private long getSectionFromTrainingDuration(SectionOnRoute sectionOnRoute) {
        long trainingSectionStartMillis = actualTraining.getMillisOn(sectionOnRoute.startIndex);
        long trainingSectionStopMillis = actualTraining.getMillisOn(sectionOnRoute.stopIndex);
        long trainingSectionDuration = trainingSectionStopMillis - trainingSectionStartMillis;
        return trainingSectionDuration;
    }

    public class SectionResults {
        public final String date;
        public final long sectionDuration;
        public final long sectionFromTrainingDuration;
        public boolean better;
        public final double distance;

        public SectionResults(String date, long sectionDuration, long sectionFromTrainingDuration, double distance) {
            this.date = date;
            this.sectionDuration = sectionDuration;
            this.sectionFromTrainingDuration = sectionFromTrainingDuration;
            this.distance = distance;
            better = isBetterTime();
        }

        private boolean isBetterTime() {
            long compareTimesResult = sectionDuration - sectionFromTrainingDuration;
            return compareTimesResult >= 0;
        }

        private long howMuchBetter() {
            long percentage = (sectionFromTrainingDuration * 100) / sectionDuration;
            percentage = percentage - 100;
            return percentage * -1;
        }

        public String sectionDurationString() {
            return Utils.millisToTimeFormat(sectionDuration);
        }

        public String sectionFromTrainingDurationString() {
            return Utils.millisToTimeFormat(sectionFromTrainingDuration);
        }

        @Override
        public String toString() {
            return String.format("" +
                    "\nData: %s " +
                    "\nDystans: %.2f m" +
                    "\nCzas odcinka: %s h " +
                    "\nCzas odcinka w tym treningu: %s h " +
                    "\nPostęp: %d procent\n",
                    date,
                    distance,
                    sectionDurationString(),
                    sectionFromTrainingDurationString(),
                    howMuchBetter());
        }
    }

    class SectionOnRoute {
        public final int startIndex;
        public final int stopIndex;
        public final Section section;

        SectionOnRoute(int startIndex, int stopIndex, Section section) {
            this.startIndex = startIndex;
            this.stopIndex = stopIndex;
            this.section = section;
        }
    }
}
