package pl.kopocinski.lukasz.gorunning.dagger.components;

import pl.kopocinski.lukasz.gorunning.dagger.annotations.PerActivity;
import pl.kopocinski.lukasz.gorunning.dagger.modules.ActivityModule;
import pl.kopocinski.lukasz.gorunning.dagger.modules.FragmentModule;
import pl.kopocinski.lukasz.gorunning.history.overview.HistoryActivityPresenter;
import pl.kopocinski.lukasz.gorunning.main.MainActivityPresenter;
import pl.kopocinski.lukasz.gorunning.results.TrainingResultsActivityPresenter;
import pl.kopocinski.lukasz.gorunning.training.activity.TrainingPresenter;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    FragmentComponent fragmentComponent(FragmentModule module);

    MainActivityPresenter mainPresenter();

    TrainingPresenter trainingActivityPresenter();

    HistoryActivityPresenter historyActivityPresenter();

    TrainingResultsActivityPresenter trainingResultsActivityPresenter();
}
