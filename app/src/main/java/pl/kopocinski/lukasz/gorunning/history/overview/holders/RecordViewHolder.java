package pl.kopocinski.lukasz.gorunning.history.overview.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pl.kopocinski.lukasz.gorunning.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecordViewHolder extends NormalViewHolder {

    @BindView(R.id.record)
    public TextView record;

    @BindView(R.id.record_image)
    public ImageView recordIcon;

    public RecordViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
