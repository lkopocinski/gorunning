package pl.kopocinski.lukasz.gorunning.database.model;

import com.google.android.gms.maps.model.LatLng;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.DATE_FORMAT_PATTERN;
import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.DAY_FORMAT_PATTERN;
import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.TIMESTAMP_FORMAT_PATTERN;
import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.TIME_FORMAT_PATTERN;
import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.toFormat;
import static pl.kopocinski.lukasz.gorunning.utils.Utils.averagePaceToString;
import static pl.kopocinski.lukasz.gorunning.utils.Utils.countAveragePace;
import static pl.kopocinski.lukasz.gorunning.utils.Utils.kmToString;
import static pl.kopocinski.lukasz.gorunning.utils.Utils.mToKm;
import static pl.kopocinski.lukasz.gorunning.utils.Utils.millisToTimeFormat;

public class Training extends RealmObject {

    public static final String ID = "id";
    public static final String DATE = "date";

    @PrimaryKey
    private String id;

    @Required
    private Date date;

    private double distanceMeters;
    private long durationMillis;

    private boolean record;
    private boolean active;

    private RealmList<Coordinates> route;

    public Training() {}

    public Training(@NonNull Date date) {
        this.id = toFormat(date, TIMESTAMP_FORMAT_PATTERN);
        this.date = date;
        this.route = new RealmList<>();
        this.record = false;
    }

    // Date
    public Date getDate() {
        return date;
    }

    public String getDayReadable() {
        return toFormat(getDate(), DAY_FORMAT_PATTERN);
    }

    public String getDateReadable() {
        return toFormat(getDate(), DATE_FORMAT_PATTERN);
    }

    public String getDateWithHourReadable() {
        return toFormat(getDate(), DATE_FORMAT_PATTERN) + " " + toFormat(getDate(), TIME_FORMAT_PATTERN);
    }

    // Distance
    public void setDistance(double distanceInMeters) {
        this.distanceMeters = distanceInMeters;
    }

    public double getDistanceMeters() {
        return distanceMeters;
    }

    public String getDistanceString() {
        return kmToString(mToKm(distanceMeters));
    }

    public void incrementDistance(double incrementValue) {
        this.distanceMeters += incrementValue;
    }

    // Duration
    public void setDuration(long durationMillis) {
        this.durationMillis = durationMillis;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

    public String getDurationString() {
        return millisToTimeFormat(durationMillis);
    }

    // AveragePace

    public long getAveragePace() {
        return countAveragePace(durationMillis, distanceMeters);
    }

    public String getAveragePaceString() {
        return averagePaceToString(getAveragePace());
    }

    public void setRecord(boolean record) {
        this.record = record;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    // Route

    public RealmList<Coordinates> getRoute() {
        return route;
    }

    public List<LatLng> getRouteLatLng() {
        List<LatLng> routeLatLng = new ArrayList<>(route.size());

        for (Coordinates coordinates : route) {
            routeLatLng.add(coordinates.toLatLng());
        }

        return routeLatLng;
    }

    public void addCoordinates(Coordinates coordinates) {
        route.add(coordinates);
    }

    public Coordinates getLastCoordinates() {
        if (route.size() > 1) {
            return route.get(route.size() - 1);
        } else {
            return null;
        }
    }

    public Coordinates getPreLastCoordinates() {
        if (route.size() > 2) {
            return route.get(route.size() - 2);
        } else {
            return null;
        }
    }

    // Others
    public boolean isRecord() {
        return record;
    }

    public boolean isActive() {
        return active;
    }

    public long getMillisOn(int point) {
        return route.get(point).getTimeNanos() / 1000000;
    }

    // Accuracy
    public float getAverageAccuracy(){
        float accuracySum = 0;
        for (Coordinates coordinates : route) {
            accuracySum += coordinates.getAccuracy();
        }

        return accuracySum / route.size();
    }
}
