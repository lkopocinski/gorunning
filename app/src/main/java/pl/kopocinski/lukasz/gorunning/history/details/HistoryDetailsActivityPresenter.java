package pl.kopocinski.lukasz.gorunning.history.details;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import pl.kopocinski.lukasz.gorunning.RouteMatcher;
import pl.kopocinski.lukasz.gorunning.database.model.Section;
import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

import static io.realm.Sort.DESCENDING;

class HistoryDetailsActivityPresenter extends MvpBasePresenter<HistoryDetailsActivityView> {

    private Realm realmInstance;

    @Inject
    HistoryDetailsActivityPresenter() {
        realmInstance = Realm.getDefaultInstance();
    }

    void loadTrainings(Date selectedTrainingDate) {
        List<Training> trainingsList = getAllTrainings();
        Training training = getSelectedTraining(selectedTrainingDate);
        int selectedTrainingIndex = getSelectedTrainingIndex(training, trainingsList);

        showTraining(trainingsList, selectedTrainingIndex);
    }

    private List<Training> getAllTrainings() {
        return realmInstance.where(Training.class).findAllSorted(Training.ID, DESCENDING);
    }

    private Training getSelectedTraining(@NonNull Date trainingDate) {
        return realmInstance
                .where(Training.class)
                .equalTo(Training.DATE, trainingDate)
                .findFirst();
    }

    private int getSelectedTrainingIndex(@NonNull Training training, @NonNull List<Training> trainingList) {
        return trainingList.indexOf(training);
    }

    private void showTraining(List<Training> trainingsList, int selectedTrainingIndex) {
        if (isViewAttached()) {
            getView().showTrainings(trainingsList, selectedTrainingIndex);
        }
    }

    public void showSectionResults(Training training) {
        Realm realmInstance = Realm.getDefaultInstance();
        final RealmResults<Section> sectionsAll = realmInstance.where(Section.class).findAll();

        RouteMatcher routeMatcher = new RouteMatcher(training, sectionsAll);
        final List<RouteMatcher.SectionResults> results = routeMatcher.getResults();

        List<String> stringsList = new ArrayList<>(results.size());

        for (RouteMatcher.SectionResults sectionResult : results) {
            stringsList.add(Objects.toString(sectionResult, null));
        }

        if (isViewAttached()) {
            getView().showSectionsResults(stringsList);
        }
    }
}
