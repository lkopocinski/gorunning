package pl.kopocinski.lukasz.gorunning.results;

import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import pl.kopocinski.lukasz.gorunning.BaseActivity;
import pl.kopocinski.lukasz.gorunning.MapImageUrlProvider;
import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.main.MainActivity;

import java.util.List;

import butterknife.BindView;

public class TrainingResultsActivity
        extends BaseActivity<TrainingResultsActivityView, TrainingResultsActivityPresenter>
        implements TrainingResultsActivityView {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.day) TextView day;
    @BindView(R.id.date_time) TextView dateTime;
    @BindView(R.id.distance) TextView distance;
    @BindView(R.id.duration) TextView duration;
    @BindView(R.id.average_pace) TextView averagePace;
    @BindView(R.id.mapImage) ImageView mapView;

    public static void start(Context context) {
        final Intent intent = new Intent(context, TrainingResultsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        presenter.showResults();
    }

    @Override
    public void setMapImage(List<LatLng> route) {
        MapImageUrlProvider urlProvider = new MapImageUrlProvider(this, route);

        Picasso.with(this)
                .load(urlProvider.getMapUrl())
                .error(R.drawable.google_maps_placeholder)
                .placeholder(R.drawable.google_maps_placeholder)
                .into(mapView);
    }

    @Override
    public void setDay(String day) {
        this.day.setText(day);
    }

    @Override
    public void setDate(String dateTime) {
        this.dateTime.setText(dateTime);
    }

    @Override
    public void setDistance(String distance) {
        this.distance.setText(distance);
    }

    @Override
    public void setDuration(String duration) {
        this.duration.setText(duration);
    }

    public void setAveragePace(String averagePace) {
        this.averagePace.setText(averagePace);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.start(this);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.results_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            presenter.onBinIconClicked();
        }
        return true;
    }

    @Override
    public void showDeleteTrainingAlert() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.delete_training_rationale)
                .setPositiveButton(R.string.delete, (dialogInterface, i) -> presenter.deleteTraining())
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {})
                .setCancelable(false)
                .show();
    }

    @Override
    public void onTrainingDeleted() {
        MainActivity.start(this);
        finish();
    }

    @NonNull
    @Override
    public TrainingResultsActivityPresenter createPresenter() {
        return getActivityComponent().trainingResultsActivityPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_training_results;
    }
}
