package pl.kopocinski.lukasz.gorunning.dagger.components;


import pl.kopocinski.lukasz.gorunning.dagger.modules.FragmentModule;
import pl.kopocinski.lukasz.gorunning.training.fragment.TrainingFragmentPresenter;

import dagger.Subcomponent;

@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    TrainingFragmentPresenter trainingFragmentPresenter();
}
