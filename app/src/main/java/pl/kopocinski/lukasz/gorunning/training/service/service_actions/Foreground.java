package pl.kopocinski.lukasz.gorunning.training.service.service_actions;

import org.parceler.Parcel;

@Parcel
public class Foreground extends ServiceAction {
    @Override
    public void perform(ServiceActions actions) {
        if (actions != null) {
            actions.doForeground();
        }
    }
}
