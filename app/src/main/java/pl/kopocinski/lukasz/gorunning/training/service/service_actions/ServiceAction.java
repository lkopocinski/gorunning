package pl.kopocinski.lukasz.gorunning.training.service.service_actions;

public abstract class ServiceAction {
    public abstract void perform(ServiceActions actions);
}
