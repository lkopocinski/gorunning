package pl.kopocinski.lukasz.gorunning.database.model;

import com.google.android.gms.maps.model.LatLng;

import android.location.Location;

import io.realm.RealmObject;

public class Coordinates extends RealmObject {

    private double latitude;
    private double longitude;
    private long elapsedRealTimeNanos;
    private float accuracy;

    public Coordinates() {}

    public Coordinates(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.elapsedRealTimeNanos = location.getElapsedRealtimeNanos();
        this.accuracy = location.getAccuracy();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public long getTimeNanos() {
        return elapsedRealTimeNanos;
    }

    public LatLng toLatLng() {
        return new LatLng(latitude, longitude);
    }

    public float getAccuracy() {
        return accuracy;
    }
}
