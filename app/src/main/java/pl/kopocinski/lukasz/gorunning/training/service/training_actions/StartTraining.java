package pl.kopocinski.lukasz.gorunning.training.service.training_actions;

import org.parceler.Parcel;


@Parcel
public class StartTraining extends TrainingAction {
    @Override
    public void perform(TrainingActions actions) {
        if (actions != null) {
            actions.startTraining();
        }
    }
}
