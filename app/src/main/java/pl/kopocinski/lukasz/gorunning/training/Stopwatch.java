package pl.kopocinski.lukasz.gorunning.training;


import com.orhanobut.logger.Logger;

import java.util.Locale;

import rx.Observable;
import rx.functions.Func1;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;


public class Stopwatch {

    private static final String TAG = Stopwatch.class.getSimpleName();

    private long startTime;
    private long endTime;
    private long pausedTime;
    private boolean isStopwatchRunning;

    public Stopwatch() {
        initStopwatch();
    }

    private void initStopwatch() {
        startTime = 0;
        endTime = 0;
        pausedTime = 0;
        isStopwatchRunning = false;
    }

    public void startStopwatch() {
        if (!isStopwatchRunning) {
            startTime = System.currentTimeMillis();
            isStopwatchRunning = true;
        } else {
            Logger.e(TAG, "start request for an already running stopwatch");
        }
    }

    public void stopStopwatch() {
        if (isStopwatchRunning) {
            endTime = System.currentTimeMillis();
            pausedTime = getElapsedTimeInMillis();
            isStopwatchRunning = false;
        } else {
            Logger.e(TAG, "stop request for a stopwatch that isn't running");
        }
    }

    public void resetStopwatch() {
        isStopwatchRunning = false;
        startTime = 0;
        endTime = 0;
        pausedTime = 0;
    }

    private long getElapsedTimeInMillis() {
        // If the stopwatch is running, the end time will be zero
        long millis;
        if (endTime > startTime) {
            millis = endTime - startTime;
        } else {
            millis = System.currentTimeMillis() - startTime;
        }

        return millis + pausedTime;
    }

    private String getElapsedTime() {
        return millisToTimeFormat(getElapsedTimeInMillis());
    }

    private String millisToTimeFormat(long millis) {
        long hours = MILLISECONDS.toHours(millis);
        long minutes = MILLISECONDS.toMinutes(millis) - MILLISECONDS.toMinutes(hours);
        long seconds = MILLISECONDS.toSeconds(millis) - MINUTES.toSeconds(minutes);

        return String.format(Locale.US, "%d:%02d:%02d", hours, minutes, seconds);
    }


    public boolean isPaused() {
        return pausedTime > 0;
    }

    public boolean isStopwatchRunning() {
        return isStopwatchRunning;
    }

    public Observable<Long> getElapsedTimeInMillisUpdates(int interval) {
        return Observable
                .interval(interval, MILLISECONDS)
                .flatMap(new Func1<Long, Observable<Long>>() {
                    @Override
                    public Observable<Long> call(Long timeInterval) {
                        return Observable.just(getElapsedTimeInMillis());
                    }
                });
    }
}
