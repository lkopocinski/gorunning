package pl.kopocinski.lukasz.gorunning.database.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.DATE_FORMAT_PATTERN;
import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.TIMESTAMP_FORMAT_PATTERN;
import static pl.kopocinski.lukasz.gorunning.utils.Timestamp.toFormat;

public class Section extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private RealmList<Coordinates> sectionRoute;

    public Section() {
        this(new RealmList<>());
    }

    public Section(RealmList<Coordinates> sectionRoute) {
        Date date = getActualDate();
        this.id = toFormat(date, TIMESTAMP_FORMAT_PATTERN);
        this.name = toFormat(date, DATE_FORMAT_PATTERN);
        this.sectionRoute = sectionRoute;
    }

    public RealmList<Coordinates> getSectionRoute() {
        return sectionRoute;
    }

    public List<LatLng> getSectionRouteLatLng() {
        List<LatLng> routeLatLng = new ArrayList<>(sectionRoute.size());

        for (Coordinates coordinates : sectionRoute) {
            routeLatLng.add(coordinates.toLatLng());
        }

        return routeLatLng;
    }

    public LatLng getStartPoint() {
        return sectionRoute.get(0).toLatLng();
    }

    public LatLng getStopPoint() {
        return sectionRoute.get(sectionRoute.size() - 1).toLatLng();
    }

    public long getStartMillis() {
        return sectionRoute.get(0).getTimeNanos() / 1000000;
    }

    public long getStopMillis() {
        return sectionRoute.get(sectionRoute.size() - 1).getTimeNanos() / 1000000;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCoordinates(Coordinates coordinates) {
        sectionRoute.add(coordinates);
    }

    private Date getActualDate() {
        return Calendar.getInstance().getTime();
    }

    public String getId() {
        return id;
    }

    public double getDistance() {
        return SphericalUtil.computeLength(getSectionRouteLatLng());
    }
}
