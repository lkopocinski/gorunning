package pl.kopocinski.lukasz.gorunning.training.service.training_actions;

public interface TrainingActions {

    void startTraining();

    void pauseTraining();

    void resumeTraining();

    void finishTraining();
}
