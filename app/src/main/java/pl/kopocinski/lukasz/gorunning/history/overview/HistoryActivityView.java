package pl.kopocinski.lukasz.gorunning.history.overview;

import com.hannesdorfmann.mosby.mvp.MvpView;

import pl.kopocinski.lukasz.gorunning.database.model.Training;

import java.util.List;


interface HistoryActivityView extends MvpView {
    void showTrainings(List<Training> trainingList);

    void showNoTrainingsInfo();
}
