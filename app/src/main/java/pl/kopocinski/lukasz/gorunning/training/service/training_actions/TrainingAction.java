package pl.kopocinski.lukasz.gorunning.training.service.training_actions;


public abstract class TrainingAction {
    public abstract void perform(TrainingActions actions);
}
