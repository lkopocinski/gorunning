package pl.kopocinski.lukasz.gorunning.map;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;
import android.support.v4.app.FragmentActivity;

import com.orhanobut.logger.Logger;
import com.tbruyelle.rxpermissions.RxPermissions;

import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class GoogleMap implements Map, OnMapReadyCallback {

    private static final String TAG = GoogleMap.class.getName();

    private com.google.android.gms.maps.GoogleMap googleMap;
    private MapParams params;

    private GoogleMap(@NonNull SupportMapFragment mapFragment, @NonNull MapParams params) {
        this.params = params;

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
        this.googleMap = googleMap;

        customizeMap();

        params.onMapReadyCallback.onMapReady();
    }

    private void customizeMap() {
        styleMap();
        enableMyLocationIfPermitted();
    }

    private void styleMap() {
        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), params.mapStyleResource));

            if (!success) {
                Logger.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Logger.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void enableMyLocationIfPermitted() {
        if (params.enableLocationModule) {
            RxPermissions.getInstance(getContext())
                    .request(ACCESS_FINE_LOCATION)
                    .subscribe(granted -> {
                        if (granted) {
                            enableMyLocation();
                        }
                    });
        }
    }

    @SuppressWarnings("MissingPermission")
    private void enableMyLocation() {
        if (googleMap != null) {
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void zoomLocation(@NonNull Coordinates coordinates, final int zoomSize) {
        final LatLng latLng = new LatLng(coordinates.getLatitude(), coordinates.getLongitude());

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomSize));
    }

    @Override
    public void hideMap() {

    }

    @Override
    public void printRoute(List<Coordinates> coordinatesList) {
        List<LatLng> latLngList = new ArrayList<>();
        for (Coordinates coordinates : coordinatesList) {
            latLngList.add(new LatLng(coordinates.getLatitude(), coordinates.getLongitude()));
        }

        PolylineOptions polyline = new PolylineOptions()
                .width(7)
                .addAll(latLngList)
                .color(R.color.polylineColor);

        googleMap.addPolyline(polyline);

        googleMap.addMarker(new MarkerOptions()
                .position(latLngList.get(0))
                .title("Start"));

        googleMap.addMarker(new MarkerOptions()
                .position(latLngList.get(latLngList.size() - 1))
                .title("Stop"));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngList.get(0), 16));
    }

    @Override
    public void printSection(List<Coordinates> coordinatesList) {
        List<LatLng> latLngList = new ArrayList<>();
        for (Coordinates coordinates : coordinatesList) {
            latLngList.add(new LatLng(coordinates.getLatitude(), coordinates.getLongitude()));
        }

        PolylineOptions polyline = new PolylineOptions()
                .width(10)
                .addAll(latLngList)
                .color(Color.MAGENTA);

        googleMap.addPolyline(polyline);

        //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngList.get(0), 16));
    }

    @Override
    public void removeCallbacks() {
        params.onMapReadyCallback = null;
    }

    private Context getContext() {
        return params.context;
    }

    public static class Builder {
        private final SupportMapFragment mapFragment;
        private final MapParams mapParams;

        public Builder(@NonNull FragmentActivity fragmentActivity, @IdRes int fragmentId) {
            mapFragment = (SupportMapFragment) fragmentActivity.getSupportFragmentManager()
                    .findFragmentById(fragmentId);

            mapParams = new MapParams();
            mapParams.context = fragmentActivity.getApplicationContext();
        }

        public Builder mapStyle(@RawRes int mapStyleResource) {
            mapParams.mapStyleResource = mapStyleResource;
            return this;
        }

        public Builder myLocationModule(boolean enableLocationModule) {
            mapParams.enableLocationModule = enableLocationModule;
            return this;
        }

        public Builder onMapReady(OnMapReadyCallback onMapReadyCallback) {
            mapParams.onMapReadyCallback = onMapReadyCallback;
            return this;
        }

        public GoogleMap build() {
            return new GoogleMap(mapFragment, mapParams);
        }
    }

    public interface OnMapReadyCallback {
        void onMapReady();
    }

    private static class MapParams {
        public Context context;
        public int mapStyleResource;
        public boolean enableLocationModule;
        public OnMapReadyCallback onMapReadyCallback;
    }
}
