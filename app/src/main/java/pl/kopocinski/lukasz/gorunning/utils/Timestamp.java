package pl.kopocinski.lukasz.gorunning.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Timestamp {

    public static final String MONTH_WORD_DATE_FORMAT_PATTERN="LLL dd, yyyy";
    public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    public static final String TIME_FORMAT_PATTERN = "HH:mm";
    public static final String DAY_FORMAT_PATTERN = "cccc";
    public static final String TIMESTAMP_FORMAT_PATTERN = "yyyyMMddHHmmss";

    public static String toFormat(Date dateTime, String format) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat(format, Locale.getDefault());
        return simpledateformat.format(dateTime);
    }
}
