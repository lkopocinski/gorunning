package pl.kopocinski.lukasz.gorunning.utils;

import com.google.android.gms.location.LocationRequest;

import android.location.Location;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

import javax.inject.Inject;

import rx.Observable;

import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;

public class LocationUtils {

    private ReactiveLocationProvider locationProvider;

    @Inject
    LocationUtils(ReactiveLocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    public Observable<Location> getUpdatedLocation(long interval) {
        return locationProvider.getUpdatedLocation(getLocationRequest(interval));
    }

    private LocationRequest getLocationRequest(long interval) {
        return LocationRequest.create()
                .setPriority(PRIORITY_HIGH_ACCURACY)
                .setInterval(interval);
    }
}
