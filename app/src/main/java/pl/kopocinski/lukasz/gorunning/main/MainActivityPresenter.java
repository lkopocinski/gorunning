package pl.kopocinski.lukasz.gorunning.main;


import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.orhanobut.logger.Logger;

import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;
import pl.kopocinski.lukasz.gorunning.utils.LocationUtils;
import pl.kopocinski.lukasz.gorunning.utils.ProviderGPS;

import javax.inject.Inject;

import rx.Subscription;


@SuppressWarnings("ConstantConditions")
public class MainActivityPresenter extends MvpBasePresenter<MainActivityView> {

    private boolean isMapReady;
    private LocationUtils locationUtils;
    private ProviderGPS providerGPS;

    private Subscription gpsEnableStatusSubscription;
    private Subscription locationSubscription;

    @Inject
    MainActivityPresenter(LocationUtils locationUtils, ProviderGPS providerGPS) {
        this.locationUtils = locationUtils;
        this.providerGPS = providerGPS;
        this.isMapReady = false;
    }

    void initialize() {
        if (isMapReady()) {
            onMapReady();
        } else {
            showMap();
        }
    }

    private void showMap() {
        if (isViewAttached()) {
            getView().showMap();
        }
    }

    void onMapReady() {
        isMapReady = true;

        findAndShowMyLocation();
        subscribeGPSStatus();
    }

    private void findAndShowMyLocation() {
        locationSubscription = locationUtils
                .getUpdatedLocation(100)
                .map(Coordinates::new)
                .subscribe(this::showCurrentLocation, this::onSubscriptionError);
    }

    private void onSubscriptionError(Throwable throwable) {
        Logger.e(throwable, "Probably no permissions granted");
    }

    private void subscribeGPSStatus() {
        gpsEnableStatusSubscription = providerGPS
                .requestUpdates()
                .retrieveEnabledStatus()
                .subscribe(gpsEnabled -> {
                    if (gpsEnabled) {
                        hideGPSDisabledInfo();
                        findAndShowMyLocation();
                    } else {
                        showGPSDisabledInfo();
                    }
                }, throwable -> {
                    Logger.e(throwable, throwable.getMessage());
                });
    }

    private void hideGPSDisabledInfo() {
        if (isViewAttached()) {
            getView().hideGPSDisabledInfo();
        }
    }


    private void showCurrentLocation(Coordinates coordinates) {
        if (isViewAttached()) {
            getView().showLocation(coordinates);
            unsubscribe(locationSubscription);
        }
    }

    private void showGPSDisabledInfo() {
        if (isViewAttached()) {
            getView().showGPSDisabledInfo();
        }
    }

    void terminate() {
        unsubscribe(locationSubscription);
        unsubscribe(gpsEnableStatusSubscription);
        providerGPS.removeUpdates();
    }

    private void unsubscribe(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    private boolean isMapReady() {
        return isMapReady;
    }
}
