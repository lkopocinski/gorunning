package pl.kopocinski.lukasz.gorunning.history.overview.holders;

import android.view.View;
import android.widget.TextView;

import pl.kopocinski.lukasz.gorunning.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NormalViewHolder extends BasicViewHolder {

    @BindView(R.id.distance)
    public TextView distance;

    public NormalViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
