package pl.kopocinski.lukasz.gorunning.history.overview.holders;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import pl.kopocinski.lukasz.gorunning.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BasicViewHolder extends ViewHolder {
    @BindView(R.id.day)
    public TextView day;

    @BindView(R.id.date)
    public TextView date;

    public BasicViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
