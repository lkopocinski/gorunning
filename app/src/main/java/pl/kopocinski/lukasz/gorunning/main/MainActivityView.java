package pl.kopocinski.lukasz.gorunning.main;

import com.hannesdorfmann.mosby.mvp.MvpView;

import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;

/**
 * Created by Łukasz on 2016-10-28.
 */

interface MainActivityView extends MvpView {

    void showMap();

    void showLocation(Coordinates coordinates);

    void showGPSDisabledInfo();

    void hideGPSDisabledInfo();

}
