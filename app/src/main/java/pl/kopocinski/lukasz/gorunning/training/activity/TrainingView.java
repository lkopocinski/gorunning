package pl.kopocinski.lukasz.gorunning.training.activity;

import com.hannesdorfmann.mosby.mvp.MvpView;

import pl.kopocinski.lukasz.gorunning.custom_views.ButtonTraining;

public interface TrainingView extends MvpView {

    void setTrainingActionsCallback(ButtonTraining.ActionsCallback actionListener);

    void showTrainingParameters();

    void showTrainingResults();

    void setButtonStopSectionEnabled(boolean enabled);

    void setButtonStartSectionEnabled(boolean enabled);

    void showStopTrainingAlert();
}
