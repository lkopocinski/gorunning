package pl.kopocinski.lukasz.gorunning.training.service;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.orhanobut.logger.Logger;

import org.parceler.Parcels;

import pl.kopocinski.lukasz.gorunning.App;
import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.database.model.Coordinates;
import pl.kopocinski.lukasz.gorunning.database.model.Section;
import pl.kopocinski.lukasz.gorunning.training.Stopwatch;
import pl.kopocinski.lukasz.gorunning.training.activity.TrainingActivity;
import pl.kopocinski.lukasz.gorunning.training.service.section_actions.SectionAction;
import pl.kopocinski.lukasz.gorunning.training.service.section_actions.SectionActions;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.ServiceAction;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.ServiceActions;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.TrainingAction;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.TrainingActions;
import pl.kopocinski.lukasz.gorunning.utils.LocationUtils;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import io.realm.Realm;
import rx.Observable;
import rx.Subscription;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.support.v4.app.NotificationCompat.PRIORITY_MAX;

public class TrainingService extends Service implements TrainingActions, ServiceActions, SectionActions {

    public static final String TRAINING_ACTION = "trainingAction";
    public static final String SERVICE_ACTION = "serviceAction";
    public static final String SECTION_ACTION = "sectionAction";

    private static final int NOTIFICATION_ID = 77;

    private Subscription durationSubscription;
    private Subscription locationSubscription;

    private Stopwatch stopwatch = new Stopwatch();
    private pl.kopocinski.lukasz.gorunning.database.model.Training training;

    @Inject
    LocationUtils locationUtils;

    @Override
    public void onCreate() {
        Logger.d("Creating service");
        ((App) getApplication()).appComponent().inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.d("Starting service");

        if (intent == null) {
            return Service.START_STICKY;
        }

        performIntentAction(intent.getExtras());

        return Service.START_STICKY;
    }

    private void performIntentAction(Bundle extras) {
        if (extras == null) {
            return;
        }

        Parcelable parcelTrainingAction = extras.getParcelable(TRAINING_ACTION);

        if (parcelTrainingAction != null) {
            TrainingAction trainingAction = Parcels.unwrap(parcelTrainingAction);
            trainingAction.perform(this);
        }

        Parcelable parcelServiceAction = extras.getParcelable(SERVICE_ACTION);

        if (parcelServiceAction != null) {
            ServiceAction serviceAction = Parcels.unwrap(parcelServiceAction);
            serviceAction.perform(this);
        }

        Parcelable parcelSectionAction = extras.getParcelable(SECTION_ACTION);

        if (parcelSectionAction != null) {
            SectionAction sectionAction = Parcels.unwrap(parcelSectionAction);
            sectionAction.perform(this);
        }
    }

    @Override
    public void startTraining() {
        stopwatch.startStopwatch();
        initTrainingInDatabase();
        startSubscriptions();
    }

    private void initTrainingInDatabase() {
        training = new pl.kopocinski.lukasz.gorunning.database.model.Training(getActualDate());
        training.setActive(true);

        saveTraining();
    }

    private void startSubscriptions() {
        durationSubscription = getDurationObservable().subscribe();
        locationSubscription = getLocationObservable().subscribe();
    }

    public Observable<Long> getDurationObservable() {
        return stopwatch
                .getElapsedTimeInMillisUpdates(1000)
                .doOnNext(this::saveDuration);
    }

    private void saveTraining() {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> realm.copyToRealm(training));
    }

    private void saveDuration(long duration) {
        training.setDuration(duration);
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> realm.copyToRealmOrUpdate(training));
    }

    private Date getActualDate() {
        return Calendar.getInstance().getTime();
    }

    private Observable<Double> getLocationObservable() {
        return locationUtils
                .getUpdatedLocation(3000)
                .map(Coordinates::new)
                .doOnNext(this::saveActualCoordinates)
                .map(this::countDistanceSinceLastLocation)
                .doOnNext(this::saveIncrementedDistance)
                .doOnError(this::onSubscriptionError);
    }

    private double countDistanceSinceLastLocation(Coordinates actualCoordinates) {
        Coordinates preLastCoordinates = training.getPreLastCoordinates();
        if (preLastCoordinates != null) {
            return countDistance(preLastCoordinates, actualCoordinates);
        } else {
            return 0.0;
        }
    }

    private void saveIncrementedDistance(double distanceSinceLastLocation) {
        training.incrementDistance(distanceSinceLastLocation);
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> realm.copyToRealmOrUpdate(training));
    }

    private void saveActualCoordinates(Coordinates actualCoordinates) {
        training.addCoordinates(actualCoordinates);
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> realm.copyToRealmOrUpdate(training));
    }

    private void onSubscriptionError(Throwable throwable) {
        Logger.e(throwable, "Probably no permissions granted");
    }

    private double countDistance(@NonNull final Coordinates from,
                                 @NonNull final Coordinates to) {
        LatLng latLngFrom = new LatLng(from.getLatitude(), from.getLongitude());
        LatLng latLngTo = new LatLng(to.getLatitude(), to.getLongitude());

        return SphericalUtil.computeDistanceBetween(latLngFrom, latLngTo);
    }

    @Override
    public void pauseTraining() {
        Logger.d("TrainingActivity Paused");
        stopwatch.stopStopwatch();
        unsubscribe(durationSubscription);
        unsubscribe(locationSubscription);
    }

    @Override
    public void resumeTraining() {
        Logger.d("TrainingActivity Resumed");
        stopwatch.startStopwatch();
        durationSubscription = getDurationObservable().subscribe();
        locationSubscription = getLocationObservable().subscribe();
    }

    @Override
    public void finishTraining() {
        stopSelf();
    }

    @Override
    public void onDestroy() {
        Logger.d("Destroying Service");

        unsubscribe(durationSubscription);
        unsubscribe(locationSubscription);

        stopwatch.stopStopwatch();
        training.setActive(false);
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> realm.copyToRealmOrUpdate(training));

        super.onDestroy();
    }

    @Override
    public void doForeground() {
        Logger.d("Foreground");
        startForeground(NOTIFICATION_ID, createNotification());
    }

    @Override
    public void doBackground() {
        Logger.d("Background");
        stopForeground(true);
    }


    private Subscription sectionSubscription;

    @Override
    public void startSection() {
        Section section = new Section();
        sectionSubscription = locationUtils
                .getUpdatedLocation(1000)
                .map(Coordinates::new)
                .map(coordinates -> {
                    section.addCoordinates(coordinates);
                    return section;
                })
                .doOnNext(this::saveSection)
                .doOnError(this::onSubscriptionError)
                .subscribe();
    }

    private void saveSection(Section section) {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> realm.copyToRealmOrUpdate(section));
    }

    @Override
    public void stopSection() {
        unsubscribe(sectionSubscription);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Notification createNotification() {
        Intent resultIntent = new Intent(this, TrainingActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent, FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_content_text))
                .setPriority(PRIORITY_MAX)
                .setSmallIcon(R.drawable.ic_runner_notification)
                .setContentIntent(resultPendingIntent);

        return builder.build();
    }

    private void unsubscribe(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }
}