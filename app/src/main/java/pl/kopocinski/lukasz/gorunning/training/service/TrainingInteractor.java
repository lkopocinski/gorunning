package pl.kopocinski.lukasz.gorunning.training.service;

import pl.kopocinski.lukasz.gorunning.training.service.section_actions.SectionActions;
import pl.kopocinski.lukasz.gorunning.training.service.service_actions.ServiceActions;
import pl.kopocinski.lukasz.gorunning.training.service.training_actions.TrainingActions;

public interface TrainingInteractor extends TrainingActions, ServiceActions, SectionActions { /* no-op */}
