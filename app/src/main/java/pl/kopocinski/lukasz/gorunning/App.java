package pl.kopocinski.lukasz.gorunning;

import android.os.StrictMode;

import com.orhanobut.logger.Logger;
import com.squareup.leakcanary.LeakCanary;

import pl.kopocinski.lukasz.gorunning.dagger.components.AppComponent;
import pl.kopocinski.lukasz.gorunning.dagger.components.DaggerAppComponent;
import pl.kopocinski.lukasz.gorunning.dagger.modules.AppModule;
import pl.kopocinski.lukasz.gorunning.dagger.modules.LocationModule;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.GINGERBREAD;


public class App extends android.app.Application {

    private static final String REALM_FILE_NAME = "GoRunningRealm";

    private static final String LOGGER_TAG = "LOG";
    private static final int METHOD_COUNT = 1;

    private static final String FONT_PATH = "fonts/Nunito-Medium.ttf";

    private final AppComponent appComponent = createComponent();

    private AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .locationModule(new LocationModule(this))
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        initLeakCanary();
        initRealm();
        initLogger();
        initCalligraphy();
    }

    private void initLeakCanary() {
        //enabledStrictMode();
        LeakCanary.install(this);
    }

    private void enabledStrictMode() {
        if (SDK_INT >= GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
    }

    private void initRealm() {
        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(REALM_FILE_NAME)
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void initLogger() {
        if (BuildConfig.DEBUG) {
            Logger.init(LOGGER_TAG).methodCount(METHOD_COUNT);
        }
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(FONT_PATH)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public AppComponent appComponent() {
        return appComponent;
    }
}
