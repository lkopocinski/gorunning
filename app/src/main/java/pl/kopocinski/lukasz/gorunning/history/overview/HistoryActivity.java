package pl.kopocinski.lukasz.gorunning.history.overview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import pl.kopocinski.lukasz.gorunning.BaseActivity;
import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.database.model.Training;
import pl.kopocinski.lukasz.gorunning.history.details.HistoryDetailsActivity;

import java.util.List;

import butterknife.BindView;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class HistoryActivity extends BaseActivity<HistoryActivityView, HistoryActivityPresenter>
        implements HistoryActivityView {

    private static final int COLUMNS_COUNT = 2;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.history_empty_view) TextView emptyView;

    private HistoryAdapter adapter;

    public static void start(Context context) {
        final Intent intent = new Intent(context, HistoryActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setUpRecyclerView();

        presenter.loadTrainings();
    }

    private void setUpRecyclerView() {
        adapter = new HistoryAdapter();
        adapter.onItemClick().subscribe(this::showTrainingDetails);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(COLUMNS_COUNT, VERTICAL));
        recyclerView.setHasFixedSize(true);
    }

    private void showTrainingDetails(Training training) {
        HistoryDetailsActivity.start(this, training);
    }

    @Override
    public void showTrainings(List<Training> trainings) {
        recyclerView.setVisibility(VISIBLE);
        emptyView.setVisibility(GONE);

        adapter.updateDataSet(trainings);
    }

    @Override
    public void showNoTrainingsInfo() {
        recyclerView.setVisibility(GONE);
        emptyView.setVisibility(VISIBLE);
    }

    @NonNull
    @Override
    public HistoryActivityPresenter createPresenter() {
        return getActivityComponent().historyActivityPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_history;
    }
}
