package pl.kopocinski.lukasz.gorunning.training.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

import pl.kopocinski.lukasz.gorunning.BaseActivity;
import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.custom_views.ButtonTraining;
import pl.kopocinski.lukasz.gorunning.custom_views.ButtonTraining.ActionsCallback;
import pl.kopocinski.lukasz.gorunning.results.TrainingResultsActivity;
import pl.kopocinski.lukasz.gorunning.training.fragment.TrainingFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class TrainingActivity extends BaseActivity<TrainingView, TrainingPresenter>
        implements TrainingView {

    public static final String NEW_TRAINING = "NEW TRAINING";
    public static final float ALPHA_ENABLED = 1.0f;
    public static final float ALPHA_DISABLED = 0.4f;

    @BindView(R.id.button_training) ButtonTraining buttonTraining;
    @BindView(R.id.button_start_section) Button buttonStartSection;
    @BindView(R.id.button_stop_section) Button buttonStopSection;

    public static void startWithNewTraining(Context context) {
        final Intent intent = new Intent(context, TrainingActivity.class);
        intent.putExtra(NEW_TRAINING, true);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.initialize(isNewTraining(getIntent()));
    }

    public boolean isNewTraining(Intent intent) {
        Bundle extras = intent.getExtras();
        return extras != null && extras.getBoolean(NEW_TRAINING);
    }

    @Override
    public void setTrainingActionsCallback(ActionsCallback actionsCallback) {
        if (buttonTraining != null) {
            buttonTraining.setActionsCallback(actionsCallback);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach();
    }

    @Override
    public void showTrainingParameters() {
        loadTrainingFragment();
    }

    public void loadTrainingFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, TrainingFragment.newInstance())
                .commit();
    }

    @Override
    public void showTrainingResults() {
        TrainingResultsActivity.start(this);
        finish();
    }

    @OnClick(R.id.button_start_section)
    void buttonStartSectionClick() {
        presenter.buttonStartSectionClicked();
    }

    @OnClick(R.id.button_stop_section)
    void buttonStopSectionClick() {
        presenter.buttonStopSectionClicked();
    }

    @Override
    public void setButtonStopSectionEnabled(boolean enabled) {
        setEnabled(buttonStopSection, enabled);
    }

    @Override
    public void setButtonStartSectionEnabled(boolean enabled) {
        setEnabled(buttonStartSection, enabled);
    }

    private void setEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        view.setAlpha(enabled ? ALPHA_ENABLED : ALPHA_DISABLED);
    }

    @Override
    public void onBackPressed() {
        presenter.onExit();
    }

    @Override
    public void showStopTrainingAlert() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.end_training_alert_title)
                .setMessage(R.string.end_training_alert_rationale)
                .setPositiveButton(R.string.finish, (dialogInterface, i) -> presenter.onAlertPositiveButtonClicked())
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> { /* no-op */ })
                .setCancelable(false)
                .show();
    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    @NonNull
    @Override
    public TrainingPresenter createPresenter() {
        return getActivityComponent().trainingActivityPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_training;
    }
}
