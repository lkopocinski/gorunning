package pl.kopocinski.lukasz.gorunning.utils;

import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

public class Utils {

    public static int dpToPx(float dp, Resources resources) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
        return (int) px;
    }

    public static double mToKm(double distanceInMeters) {
        return distanceInMeters / 1000;
    }

    public static String kmToString(double distanceInKilometers) {
        return new DecimalFormat("##.##").format(distanceInKilometers).replace(",", ".");
    }

    public static long countAveragePace(long timeInMillis, double distanceInMeters) {
        if (distanceInMeters == 0) return 0;

        long timeInSeconds = MILLISECONDS.toSeconds(timeInMillis);
        double distanceInKm = distanceInMeters / 1000;
        return (long) (timeInSeconds / distanceInKm);
    }

    public static String averagePaceToString(long averagePace) {
        long minutes = SECONDS.toMinutes(averagePace);
        long seconds = averagePace - MINUTES.toSeconds(minutes);

        return String.format(Locale.US, "%01d:%02d", minutes, seconds);
    }

    public static String millisToTimeFormat(long millis) {
        long hours = MILLISECONDS.toHours(millis);
        long minutes = MILLISECONDS.toMinutes(millis) - MILLISECONDS.toMinutes(hours);
        long seconds = MILLISECONDS.toSeconds(millis) - MINUTES.toSeconds(minutes);

        return String.format(Locale.US, "%d:%02d:%02d", hours, minutes, seconds);
    }

}
