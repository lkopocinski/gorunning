package pl.kopocinski.lukasz.gorunning.history.overview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.kopocinski.lukasz.gorunning.R;
import pl.kopocinski.lukasz.gorunning.database.model.Training;
import pl.kopocinski.lukasz.gorunning.history.overview.holders.BasicViewHolder;
import pl.kopocinski.lukasz.gorunning.history.overview.holders.NormalViewHolder;
import pl.kopocinski.lukasz.gorunning.history.overview.holders.RecordViewHolder;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int NORMAL_TYPE = 0;
    private final static int RECORD_TYPE = 1;
    private final static int NO_DISTANCE_TYPE = 2;

    private List<Training> trainingList = new ArrayList<>();
    private final PublishSubject<Training> onItemClickSubject = PublishSubject.create();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case RECORD_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_record, parent, false);
                return new RecordViewHolder(view);
            case NORMAL_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_normal, parent, false);
                return new NormalViewHolder(view);
            case NO_DISTANCE_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_no_distance, parent, false);
                return new BasicViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_normal, parent, false);
                return new NormalViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Training training = trainingList.get(position);

        switch (holder.getItemViewType()) {
            case RECORD_TYPE:
                bindRecord((RecordViewHolder) holder, training);
                break;
            case NORMAL_TYPE:
                bindNormal((NormalViewHolder) holder, training);
                break;
            case NO_DISTANCE_TYPE:
                bindBasic((BasicViewHolder) holder, training);
                break;
        }
    }

    private void bindRecord(RecordViewHolder holder, Training training) {
        bindNormal(holder, training);

        holder.recordIcon.setImageResource(R.drawable.trophy);
        holder.record.setText(R.string.new_record);
    }

    private void bindNormal(NormalViewHolder holder, Training training) {
        bindBasic(holder, training);

        String distance = String.format("%s km", training.getDistanceString());

        holder.distance.setText(distance);
    }

    private void bindBasic(BasicViewHolder holder, Training training) {
        holder.day.setText(training.getDayReadable());
        holder.date.setText(training.getDateReadable());

        holder.itemView.setOnClickListener(view -> onItemClickSubject.onNext(training));
    }

    @Override
    public int getItemViewType(int position) {
        Training training = trainingList.get(position);

        if (training.isRecord()) {
            return RECORD_TYPE;
        } else if (training.getDistanceMeters() < 0.01) {
            return NO_DISTANCE_TYPE;
        } else {
            return NORMAL_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return trainingList.size();
    }

    void updateDataSet(List<Training> newTrainingList) {
        trainingList.clear();
        trainingList.addAll(newTrainingList);
        notifyDataSetChanged();
    }

    Observable<Training> onItemClick() {
        return onItemClickSubject.asObservable();
    }
}
